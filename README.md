# sysfs

A python interface to the sysfs on Linux. Each node may be treated as either
a dictionary or an object, both methods redirect to reading and writing to
the file system. There is intentionally no caching or protections.

Be aware of what you are doing with the sysfs. Assigning to the wrong
attribute or key on the wrong node with write permissions can be detrimental
to your system, if you don't know what you are doing.

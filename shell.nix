{ pkgs ? import <nixpkgs> { } }:
with pkgs;
let
  poetry2nix = import (pkgs.fetchFromGitHub {
    owner = "nix-community";
    repo = "poetry2nix";
    rev = "bb06c87e893ac39bde3773a70b60674dde15654e";
    sha256 = "1kj2ckrsy601ixnzbwqchj8gdyf699pcd8j5dqda9250ms34zck1";
  }) { };
  poetryEnv = poetry2nix.mkPoetryEnv {
    projectDir = ./.;
    python = python3;
  };
in stdenv.mkDerivation {
  name = "env";
  buildInputs = [ poetry poetryEnv bashInteractive ];
}
